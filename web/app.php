<?php

return function () {

    require_once '/../vendor/autoload.php';

    $app = new \Silex\Application();

    return $app;
};