<?php

require_once "phing/Task.php";

class TestAdd extends Task {

    private $name = null;
    private $todo = null;

    public function setName($str) {
        $this->name = $str;
    }

    public function setTodo($str) {
        $this->todo = $str;
    }

    public function init() {
        // nothing to do here
    }

    public function main() {
        print($this->name.'\n');
        print($this->todo.'\n\n');
    }
}

?>